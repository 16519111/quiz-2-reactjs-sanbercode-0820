function filterBooksPromise(colorful, amountOfPage){
    return new Promise(function(resolve, reject) {
        var books = [
            {name: "shinchan", totalPage: 50, isColorful: true},
            {name: "Kalkulas", totalPage: 250, isColorful: false},
            {name: "doraemon", totalPage: 40, isColorful: true},
            {name: "shinchan", totalPage: 300, isColorful: false}
        ]
        if(amountOfPage > 0){
            resolve(books.filter(x => x.totalPage >= amountOfPage && x.isColorful == colorful));
        } else {
            var reason = new Error("maaf parameter salah")
            reject(reason);
        }
    })
}

function tampilPromise() {
    filterBooksPromise(false,60)
        .then(function (fulfilled) {
            console.log(fulfilled);
        })
        .catch(function (error) {
            console.log(error.message);
        });
}


 
tampilPromise()