let warna = ["biru","merah","kuning","hijau"];

let databukuTambahan = {
    penulis: "john doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemrograman dasar",
    jumlahHalaman: 172,
    warnaSampul: ["Hitam"]
}

buku.warnaSampul = [...buku.warnaSampul,...warna];
buku = {...buku,...databukuTambahan};

console.log(buku);