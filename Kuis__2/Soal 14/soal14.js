// Bila yang dimaksud soal merupakan 1 fungsi serbaguna
const volume = (...argument) => {
    if(argument.length == 1){
        return argument[0]*argument[0]*argument[0];
    }
    else{
        return argument[0]*argument[1]*argument[2];
    }
}

console.log(`${volume(7)}`);
console.log(`${volume(2,4,5)}`);

// Bila yang dimaksud soal merupakan 2 fungsi berbeda

const kubus = (...argument) => {
    return argument[0]*argument[0]*argument[0];
}

const balok = (...argument) => {
    return argument[0]*argument[1]*argument[2];
}

console.log(`${kubus(7)}`);
console.log(`${balok(2,4,5)}`);