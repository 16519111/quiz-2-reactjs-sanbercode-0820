class BangunDatar {
    constructor(nama){
        this.nama = nama;
    }
    luas(){
        return "";
    }
    keliling(){
        return "";
    }
}

class Lingkaran extends BangunDatar {
    constructor(nama){
        super(nama)
    }
    luas(r){
        return 3.145*r*r;
    }
    keliling(r){
        return 3.145*2*r;
    }
    get name() {
        return this.nama;
    }
    set name(x) {
        this.nama = x;
    } 
}

class Persegi extends BangunDatar {
    constructor(nama){
        super(nama)
    }
    luas(r){
        return r*r;
    }
    keliling(r){
        return 4*r;
    }
    get name() {
        return this.nama;
    }
    set name(x) {
        this.nama = x;
    } 
}

var A = new Lingkaran("C");
console.log(A.keliling(7));
console.log(A.luas(7));
A.name = "B";
console.log(A.name);

var B = new Persegi("D");
console.log(B.keliling(2));
console.log(B.luas(2));