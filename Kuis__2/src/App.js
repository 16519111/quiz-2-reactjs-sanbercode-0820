import React from 'react';
import logo from './logo.svg';
import './App.css';
import Tampilan from './Tampilan';

function App() {
  return (
    <div className="App">
      <Tampilan/>
    </div>
  );
}

export default App;
