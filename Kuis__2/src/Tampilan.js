import React from 'react';
import './App.css';

function App() {
    const data = 
    [{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }]


  return (
    <div className="App">
      <header className="App-header">
      </header>
      <body>
        <div style={{display: "flex", justifyContent:"center"}}>
            <div style={{padding:'1%'}}>
                <div style={{border: "2px solid", borderRadius: "10px", marginBottom:'3%'}}>
                    <div>
                        <img src={data[0].photo} style={{maxWidth: '600px'}}/>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        <strong>{`Mr. ${data[0].name}`}</strong>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {data[0].profession}
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {`${data[0].name} years old`}
                    </div>
                </div>
                <div style={{border: "2px solid", borderRadius: "10px"}}>
                    <div>
                        <img src={data[2].photo} style={{maxWidth: '600px'}}/>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        <strong>{`Mr. ${data[2].name}`}</strong>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {data[2].profession}
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {`${data[2].name} years old`}
                    </div>
                </div>
            </div>
            <div style={{padding:'1%'}}>
                <div style={{border: "2px solid", borderRadius: "10px", marginBottom:'3%'}}>
                    <div>
                        <img src={data[1].photo} style={{maxWidth: '600px'}}/>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        <strong>{`Mr. ${data[1].name}`}</strong>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {data[1].profession}
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {`${data[1].name} years old`}
                    </div>
                </div>
                <div style={{border: "2px solid", borderRadius: "10px"}}>
                    <div>
                        <img src={data[3].photo} style={{maxWidth: '265px'}}/>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        <strong>{`Mr. ${data[3].name}`}</strong>
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {data[3].profession}
                    </div>
                    <div style={{display: "flex", justifyContent:"flex-start", marginLeft:'1%',marginTop:'1%',marginBottom:'1%'}}>
                        {`${data[3].name} years old`}
                    </div>
                </div>
            </div>
        </div>
      </body>   
    </div>
    
  );
}

export default App;